# SDP Prototype - Deprecated

This repository is no longer used to develop the SDP prototype source code. A
large number of software artefacts were being generated from this repository
and it was becoming difficult to maintain. The code and documentation have been
moved to the new repositories shown in the table below. The overview
documentation has been moved to the
[SDP Integration](https://gitlab.com/ska-telescope/sdp-integration) repository.

| Artefact             | Location in this repository | New repository                                                                     |
|-----------------------|-----------------------------|-----------------------------------------------------------------------------------|
| SDP Helm chart        | `charts`                    | [sdp-integration](https://gitlab.com/ska-telescope/sdp-integration)               |
| Configuration library | `src/config_db`             | [sdp-config](https://gitlab.com/ska-telescope/sdp-config)                         |
| Logging library       | `src/logging`               | [sdp-logging](https://gitlab.com/ska-telescope/sdp-logging)                       |
| LMC (Tango devices)   | `src/lmc`                   | [sdp-lmc](https://gitlab.com/ska-telescope/sdp-lmc)                               |
| Processing controller | `src/processing_controller` | [sdp-proccontrol](https://gitlab.com/ska-telescope/sdp-proccontrol)               |
| Helm deployer         | `src/helm_deploy`           | [sdp-helmdeploy](https://gitlab.com/ska-telescope/sdp-helmdeploy)                 |
| Helm deployer charts  | `src/helm_deploy/charts`    | [sdp-helmdeploy-charts](https://gitlab.com/ska-telescope/sdp-helmdeploy-charts)   |
| Console               | `src/console`               | [sdp-console](https://gitlab.com/ska-telescope/sdp-console)                       |
| Workflows             | `src/workflows`             | [sdp-workflows-procfunc](https://gitlab.com/ska-telescope/sdp-workflows-procfunc) |
| Visibility receive    | `src/vis_receive`           | [sdp-workflows-procfunc](https://gitlab.com/ska-telescope/sdp-workflows-procfunc) |
| PSS receive           | `src/pss_receive`           | [sdp-workflows-procfunc](https://gitlab.com/ska-telescope/sdp-workflows-procfunc) |
