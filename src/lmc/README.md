# SKA SDP Local Monitoring and Control

This package contains the Tango device servers to control the SKA SDP
prototype.

Install with:

```bash
pip install ska-sdp-lmc
```

If you have Tango set up locally, you can run the devices with:

```bash
SDPMaster <instance name> [-v4]
```

or:

```bash
SDPSubarray <instance name> [-v4]
```
