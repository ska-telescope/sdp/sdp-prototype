Getting Started
===============

I want to..
-----------

Understand the design of the SDP prototype
++++++++++++++++++++++++++++++++++++++++++

Documentation can be found at:

    - :doc:`design/design`
    - :doc:`design/components`
    - :doc:`design/module`


Set up the SDP prototype in a local development environment
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Instructions can be found at :doc:`running/setting_up_local_dev_env`.

Run the SDP prototype stand-alone
+++++++++++++++++++++++++++++++++

First you need to make sure the local development environment is set up.
The details on how to run the prototype stand-alone can be found at
:doc:`running/running_standalone`.

Run the SDP Prototype in the integration environment
++++++++++++++++++++++++++++++++++++++++++++++++++++

Details can be found at :doc:`running/running_integration`.

Find out about the SDP Tango devices
++++++++++++++++++++++++++++++++++++

Details on the interface and Python API for the SDP Master device
can be found at :doc:`lmc/sdp_master`.

Details on the interface and Python API for the SDP Subarray device can be found at
:doc:`lmc/sdp_subarray`.

Know more about the SDP configuration database
++++++++++++++++++++++++++++++++++++++++++++++

An overview on how to access SKA SDP configuration information can be found
at :doc:`configuration/config_db`.

Details on the configuration schema can be found at :doc:`configuration/config_schema`.

API details of the configuration database can be found here :doc:`configuration/config_api`.

Understand the design of the services
+++++++++++++++++++++++++++++++++++++

The documentation on the processing controller service can be found at :doc:`services/processing_controller`.

The documentation on the Helm deployer service can be found at :doc:`services/helm_deployer`.

Run workflows
+++++++++++++

Instructions on how to run the visibility receive workflow can be found at :doc:`workflows/vis_receive`.

Details on how to run the PSS receive can be found at :doc:`workflows/pss_receive`.

Instructions on how to run the test workflows can be found at :doc:`workflows/test_workflows`.

Develop a workflow
++++++++++++++++++

Instructions on how to develop and test a workflow can be found at :doc:`workflows/workflow_development`.
